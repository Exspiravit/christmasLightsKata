import ChristmasLights from "../src/ChristmasLights";

// generate matrix test

test('Generate matrix for 1*1', () => {
    // (Arrange) - (Act)
    const christmasLights = new ChristmasLights(1);
    // (Assert)
    expect(christmasLights.matrix).toEqual([[0]]);
});

test('Generate matrix for 2*2', () => {
    // (Arrange) - (Act)
    const christmasLights = new ChristmasLights(2);
    // (Assert)
    expect(christmasLights.matrix).toEqual([
        [0, 0],
        [0, 0]
    ]);
});

test('Turn On all lights', () => {
    // (Arrange)
    const christmasLights = new ChristmasLights(2);
    // (Act)
    christmasLights.turnOn([[0,0],[1,1]])
    // (Assert)
    expect(christmasLights.matrix).toEqual([
        [1, 1],
        [1, 1]
    ]);
})

test('Turn On one light', () => {
    // (Arrange)
    const christmasLights = new ChristmasLights(2);
    // (Act)
    christmasLights.turnOn([[0,0],[0,0]])
    // (Assert)
    expect(christmasLights.matrix).toEqual([
        [1, 0],
        [0, 0]
    ]);
})

test('Turn On ligthts in order', () => {
    // (Arrange)
    const christmasLights = new ChristmasLights(3);
    // (Act)
    christmasLights.turnOn([[0,1],[1,2]])
    // (Assert)
    expect(christmasLights.matrix).toEqual([
        [0, 1, 1],
        [0, 1, 1],
        [0, 0, 0]
    ]);
})

test('Turn On ligthts in disorder', () => {
    // (Arrange)
    const christmasLights = new ChristmasLights(3);
    // (Act)
    christmasLights.turnOn([[1,1],[0,2]])
    // (Assert)
    expect(christmasLights.matrix).toEqual([
        [0, 1, 1],
        [0, 1, 1],
        [0, 0, 0]
    ]);
})

test('Turn Off all lights', () => {
    // (Arrange)
    const christmasLights = new ChristmasLights(2,1);
    // (Act)
    christmasLights.turnOff([[0,0],[1,1]])
    // (Assert)
    expect(christmasLights.matrix).toEqual([
        [0, 0],
        [0, 0]
    ]);
})

test('Turn Off one light', () => {
    // (Arrange)
    const christmasLights = new ChristmasLights(2,1);
    // (Act)
    christmasLights.turnOff([[0,0],[0,0]])
    // (Assert)
    expect(christmasLights.matrix).toEqual([
        [0, 1],
        [1, 1]
    ]);
})

test('Turn Off ligthts in order', () => {
    // (Arrange)
    const christmasLights = new ChristmasLights(3,1);
    // (Act)
    christmasLights.turnOff([[0,1],[1,2]])
    // (Assert)
    expect(christmasLights.matrix).toEqual([
        [1, 0, 0],
        [1, 0, 0],
        [1, 1, 1]
    ]);
})

test('Turn Off ligthts in disorder', () => {
    // (Arrange)
    const christmasLights = new ChristmasLights(3,1);
    // (Act)
    christmasLights.turnOff([[1,1],[0,2]])
    // (Assert)
    expect(christmasLights.matrix).toEqual([
        [1, 0, 0],
        [1, 0, 0],
        [1, 1, 1]
    ]);
})

test('Toggle central light', () => {
    // (Arrange)
    const christmasLights = new ChristmasLights(3);
    // (Act)
    christmasLights.toggle([[1,1],[1,1]])
    // (Assert)
    expect(christmasLights.matrix).toEqual([
        [0, 0, 0],
        [0, 1, 0],
        [0, 0, 0]
    ]);
})

//  kata instruction test #1

test('Kata instruction test', () => {
    // (Arrange)
    const christmasLights = new ChristmasLights(1000);
    // (Act)
    christmasLights.turnOn([[887,9],[959,629]]);
    christmasLights.turnOn([[454,398],[844,448]]);
    christmasLights.turnOff([[539,243],[559,965]]);
    christmasLights.turnOff([[370,819],[676,868]]);
    christmasLights.turnOff([[145,40],[370,997]]);
    christmasLights.turnOff([[301,3],[808,453]]);
    christmasLights.turnOn([[351,678],[951,908]]);
    christmasLights.toggle([[720,196],[897,994]]);
    christmasLights.toggle([[831,394],[904,860]]);
    // (Assert)
    expect(christmasLights.calculateLights()['1']).toBe(230022);
    
})

test('Kata instruction test', () => {
    // (Arrange)
    const christmasLights = new ChristmasLights(10);
    // (Act) - (Assert)
    christmasLights.turnOn([[0,0],[5,5]]);
    expect(christmasLights.calculateLights()['1']).toBe(36)
    christmasLights.turnOff([[0,5],[5,5]]);
    expect(christmasLights.calculateLights()['1']).toBe(30)
    christmasLights.turnOff([[3,3],[3,3]]);
    expect(christmasLights.calculateLights()['1']).toBe(29)
    christmasLights.turnOn([[9,9],[8,8]]);
    expect(christmasLights.calculateLights()['1']).toBe(33)
    christmasLights.toggle([[0,0],[9,9]]);
    expect(christmasLights.calculateLights()['1']).toBe(67)
    
})

//  kata instruction test #2

test('brightness regulation with one ligth', () => {
    // (Arrange)
    const christmasLights = new ChristmasLights(100,null,true);
    // (Act)
    christmasLights.turnOn([[0,0],[0,0]]);
    // (Assert)
    expect(christmasLights.calculateLights()).toBe(1)
})

test('brightness regulation with all ligth', () => {
    // (Arrange)
    const christmasLights = new ChristmasLights(1000,null,true);
    // (Act)
    christmasLights.toggle([[0,0],[999,999]]);
    // (Assert)
    expect(christmasLights.calculateLights()).toBe(2000000)
})