export default class ChristmasLights {
    public matrix: number[][];
    private brightnessRegulator: boolean|null;

    constructor(matrixDimension: number, initialState: number = 0, brightnessRegulator: boolean|null = null) {
        const rows = new Array(matrixDimension).fill(undefined);
        const cols = new Array(matrixDimension).fill(initialState);
        this.matrix = rows.map(() => [...cols]);
        this.brightnessRegulator = brightnessRegulator;
    }
    
    /**
     * turnOn
     */
    public turnOn(coordinates: number[][]): void {
        this.toggleMatrixRectangle(coordinates,1);
    }

    /**
     * turnOf
     */
    public turnOff(coordinates: number[][]): void {
        this.toggleMatrixRectangle(coordinates,-1);
    }

    /**
     * toggle
     */
    public toggle(coordinates: number[][]): void {
        this.toggleMatrixRectangle(coordinates);
    }

    /**
     * calculateLights
     */
    public calculateLights(): any {
        const flattenMatrix = ([] as number[]).concat(...this.matrix);
        if (this.brightnessRegulator) {
            return flattenMatrix.reduce((acc: number, value) => acc + value,0)
        }else {
            return flattenMatrix.reduce((acc: any , value) => ({
                ...acc,
                [value]: (acc[value] || 0) + 1
             }), {});
        }
    }

    private toggleMatrixRectangle(coordinates: number[][], desiredState: number|null = null): void {
        const [[firstX, firstY], [SecondX, SecondY]] = coordinates;
        const minX = Math.min(firstX, SecondX);
        const minY = Math.min(firstY, SecondY);
        const maxX = Math.max(firstX, SecondX);
        const maxY = Math.max(firstY, SecondY);

        for (let i = minX; i <= maxX; i++) {
            for (let j = minY; j <= maxY; j++) {
                this.matrix[i][j] = desiredState === null 
                    ? this.brightnessRegulator 
                        ? this.matrix[i][j] + 2
                        : (this.matrix[i][j] === 1 ? 0 : 1)
                    : this.matrix[i][j] + desiredState !== -1
                        ? this.matrix[i][j] + desiredState
                        : 0;
            }
        }
    }
}